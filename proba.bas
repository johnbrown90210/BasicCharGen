﻿#Lang "qb"
begin:
Cls
RANDOMIZE TIMER

'ФЛАГИ'

npccount = 0
questnumber = 0
itemsamount = 5

'МАССИВЫ ДАННЫХ для игрока и НИП'

DIM race$(100)
DIM profession$(100)
DIM weapons$(100)
DIM equipweapon$(100)
DIM equiparmour$(100)
DIM equipshield$(100)
DIM equipjewelery$(100)
DIM transports$(100)
DIM itemss$(101, itemsamount)
DIM money(100)
DIM quests$(6)
Dim armours$(100)

print
Print tab(35); "ГЕНЕРАТОР ПЕРСОНАЖА": Print
print tab(13); "(Алексей Галкин (johnbrown90210 at gmail.com), Москва, 2012-2015)": print
INPUT "Ваше имя"; namepc$

'ГЕНЕРАТОР МЕСТА РОЖДЕНИЯ'

DATA в лесу,в пустыне,в горах,в приюте,в пещере,в семье лавочника,в королевской семье,в семье крестьян,в семье бедняков
DIM birthplace$(8)
FOR i = 0 TO 8
READ birthplace$(i)
NEXT i
birthplacecount = INT(RND * 9)

'ГЕНЕРАТОР ВЫБОРА ГОДА РОЖДЕНИЯ'

DATA Дюжей Змеи,Бородавочника,Пятнистого Ягуара,Красного Петуха,Серебряной Антилопы,Паука,Виноградной Лозы,Деревяной Колесницы,Белого Вола,Орла
DIM year$(9)
FOR i = 0 TO 9
READ year$(i)
NEXT i
yearcount = INT(RND * 10)

'ГЕНЕРАТОР ХАРАКТЕРА'

DATA были спокойным ребенком,были жестоки с окружающими,мечтали изменить мир,мечтали править,мечтали помогать окружающим,часто испытывали унижения,часто испытывали лишения,часто дрались,часто болели,были здоровым ребенком
DIM character$(9)
FOR i = 0 TO 9
READ character$(i)
NEXT i
charactercount = INT(RND * 10)

'ГЕНЕРАТОР ГЛАЗ'
DATA голубые,зеленые,карие,красные,желтые,синие,серые
DIM eyes$(6)
FOR i = 0 TO 6
READ eyes$(i)
NEXT i
eyescount = INT(RND * 7)

'ГЕНЕРАТОР ТЕЛОСЛОЖЕНИЯ'
DATA крепкое,хрупкое,атлетическое,дряблое,больное,израненное,мощное
DIM body$(6)
FOR i = 0 TO 6
READ body$(i)
NEXT i
bodycount = INT(RND * 7)

'**ВЫБОР ПЕРСОНАЖА**'

PRINT "Сгенерировать случайного персонажа? (Да/Нет)"
DO
playgeneratedpc$ = INKEY$
SELECT CASE playgeneratedpc$
CASE "д"
playgeneratedpc$ = "Да"
CLS
GOTO pregenerate
CASE "н"
CLS
CASE CHR$(27)
end
END SELECT
LOOP UNTIL playgeneratedpc$ = "д" OR playgeneratedpc$ = "н" OR playgeneratedpc$ = CHR$(27)
pcgenerate:


'ГЕНЕРАТОР ВЫБОРА РАСЫ'
pregenerate:
DATA Человек,Хоббит,Древень,Фея,Орк,Полуорк,Эльф,Дикий Эльф,Полуэльф,Высший Эльф,Светлый Эльф
DATA Темный Эльф,Речной Эльф,Пещерный Эльф,Морской Эльф,Пещерный Эльф,Гоблин,Тролль,Драконоид
DATA Гигант,Гном,Дварф,Лесной Гном,Горный Гном,Равнинный Гном,Вампир,Оборотень,Кобольд
DIM racepc$(27)
FOR i = 0 TO 27
READ racepc$(i)
NEXT i
racepccount = INT(RND * 28)
race$(npccount) = racepc$(racepccount)

'ГЕНЕРАТОР ПРЕДМЕТОВ. 0-6 еда,7-14 инструменты,15-31 зелья,32-35 разное,'
'36-43 книги,44-49 ингридиенты'

DATA Яблоко,Хлеб,Пирог,Эльфийский Хлеб,Сосиска,Вино,Пиво
DATA Лопата,Кирка,Иголка,Моток Нити,Щипцы,Удочка,Сеть,Сачок
DATA Отвар Храбрости,Напиток Скорости,Зелье Подвижности,Зелье Лечения Ран,Зелье Лечения Отравления
DATA Яд,Снотворное,Слабительное,Зелье Ускоренного заживления ран,Напиток Невидимости,Зелье Слепоты
DATA Эликсир Очарования,Жидкий Огонь,Жидкий Холод,Жидкий Туман,Зелье Парализации,Напиток Силы
DATA Карта,Факел,Пергамент,Пишущее Перо
DATA История Алодона - Книга III,Песни о Народах Алодона,Книга Сотворения,Свиток Создания Предмета
DATA Свиток Призыва Существа,Свиток Уталения Голода,Свиток Усмерения,Свиток Телепортации
DATA Болотник,Глаз Ящерицы,Кровь Жука,Сей-Трава,Крылья Бабочки,Серая Роса
DIM item$(49)
FOR i = 0 TO 49
READ item$(i)
NEXT i

'Формируем массив инвентаря для персонажа и НИП. i-порядковый номер НИП,'
'начиная с 0 для персонажа; j-порядковый номер инвентаря,начиная с 1 для'
'удобного воспроизведения'

itemsamount = INT(RND * 6)
FOR j = 1 TO itemsamount
itemcount = INT(RND * 50)
itemss$(npccount, j) = item$(itemcount)
NEXT

'ГЕНЕРАТОР ПРОФЕССИИ'
'0-21 им.падеж,22-43 дат.падеж,44-65 вин.падеж

DATA Командующий Армией,Оруженосец,Рыцарь,Солдат,Разведчик,Гонец,Пират,Капитан,Матрос,Раб,Король,Волшебник,Чародей,Ученик Чародея,Монах,Аколит,Чернокнижник,Священник Аала,Священник Каа,Священник Эроса,Священник Элуора,Священник Эросила
DATA Командующему Армией,Оруженосцу,Рыцарю,Солдату,Разведчику,Гонцу,Пирату,Капитану,Матросу,Рабу,Королю,Волшебнику,Чародею,Ученику Чародея,Монаху,Аколиту,Чернокнижнику,Священнику Аала,Священнику Каа,Священнику Эроса,Священнику Элуора,Священнику Эросила
DATA Командующего Армией,Оруженосца,Рыцаря,Солдата,Разведчика,Гонца,Пирата,Капитана,Матроса,Раба,Короля,Волшебника,Чародея,Ученика Чародея,Монаха,Аколита,Чернокнижника,Священника Аала,Священника Каа,Священника Эроса,Священника Элуора,Священника Эросила
DIM prof$(65)
FOR i = 0 TO 65
READ prof$(i)
NEXT i
profcount = INT(RND * 22)
profession$(npccount) = prof$(profcount)

'ГЕНЕРАТОР ТРАНСПОРТА 7'
DATA Конь,Осел,Летающий Ковер,Волшебная Метла,Лодка,Корабль,Воздушный Шар,Летучий Корабль
DIM transport$(7)
FOR i = 0 TO 7
READ transport$(i)
NEXT i
transportcount = INT(RND * 8)
transports$(npccount) = transport$(transportcount)
transportcheck = RND
IF transportcheck > .33 THEN transports$(npccount) = ""

'ГЕНЕРАТОР ДЕНЕГ'
money(npccount) = INT(RND * 1001)

'ОРУЖИЕ 0-6 дательный падеж,7 средний род,8-23 мужской род,24-31 женский род'

DATA Булаву,Палку,Пращу,Пику,Палицу,Секиру,Алебарду,Копье,Стилет,Палаш,Бумеранг,Трезубец,Меч,Длинный Меч,Короткий Меч,Кинжал,Кортик,Лук,Длинный Лук,Короткий Лук,Арбалет,Тесак,Кнут,Молот,Булава,Палка,Праща,Плеть,Пика,Палица,Секира,Алебарда
DIM weapon$(31)
FOR i = 0 TO 31
READ weapon$(i)
NEXT i
weaponcount = 7 + INT(RND * 25)
weapons$(npccount) = weapon$(weaponcount)

'Item_prefix_ГЕНЕРА ТОР ПРЕФИКСОВ 0-29мужской род,30-59женский род,60-89средний род,90-119Мн.ч'

DATA Говорящий,Невидимый,Светящийся,Роскошный,Каменный,Заостренный
DATA Шипованный,Благословленный,Гоблинский,Орочий,Троллий,Гномий,Эльфийский
DATA Проклятый,Сломанный,Старый,Потертый,Новый,Сияющий,Бронзовый,Железный
DATA Стальной,Серебряный,Золотой,Мифриловый,Адамантовый,Мистовый,Призрачный
DATA Радужный,Огненный
DATA Говорящая,Невидимая,Светящаяся,Роскошная,Каменная,Заостренная
DATA Шипованная,Благословленная,Гоблинская,Орочья,Троллья,Гномья,Эльфийская
DATA Проклятая,Сломанная,Старая,Потертая,Новая,Сияющая,Бронзовая,Железная
DATA Стальная,Серебряная,Золотая,Мифриловая,Адамантовая,Мистовая,Призрачная
DATA Радужная,Огненная
DATA Говорящее,Невидимое,Светящеесе,Роскошное,Каменное,Заостренное
DATA Шипованное,Благословленное,Гоблинское,Орочье,Троллье,Гномье,Эльфийское
DATA Проклятое,Сломанное,Старое,Потертое,Новое,Сияющее,Бронзовое,Железное
DATA Стальное,Серебряное,Золотое,Мифриловое,Адамантовое,Мистовое,Призрачное
DATA Радужное,Огненное
DATA Говорящие,Невидимые,Светящиеся,Роскошные,Каменные,Заостренные
DATA Шипованные,Благословленные,Гоблинские,Орочьи,Тролльи,Гномьи,Эльфийские
DATA Проклятые,Сломанные,Старые,Потертые,Новые,Сияющие,Бронзовые,Железные
DATA Стальные,Серебряные,Золотые,Мифриловые,Адамантовые,Мистовые,Призрачные
DATA Радужные,Огненные
DIM itemprefix$(119)
FOR i = 0 TO 119
READ itemprefix$(i)
NEXT i

'ПРЕФИКС МУЖСКОЙ РОД'
IF weaponcount > 7 AND weaponcount < 24 THEN GOSUB male
IF weaponcount > 23 THEN GOSUB female
IF weaponcount = 7 THEN GOSUB middle

wcount = count
GOTO armour

'ПРЕФИКС МУЖСКОЙ РОД'

male:
DO
count = INT(RND * 120)
LOOP UNTIL count < 30
RETURN

'ПРЕФИКС ЖЕНСКИЙ РОД'

female:
DO
count = INT(RND * 120)
LOOP UNTIL count > 29 AND count < 60
RETURN

'ПРЕФИКС СРЕДНИЙ РОД'

middle:
DO
count = INT(RND * 120)
LOOP UNTIL count > 59 AND count < 90
RETURN
many:
DO
count = INT(RND * 120)
LOOP UNTIL count > 89 AND count < 120
RETURN

'МАССИВ ДОСПЕХОВ. 0-3 мужской род,4-7 женский род,8-10 Мн.ч'

armour:
DATA Ламелляр,Ламинар,Чешуйчатый доспех,Кольчато-пластинчатый доспех,Бригантина,Кольчуга,Стеганка,Кираса,Латы,Наручи,Поножи
DIM armour$(10)
FOR i = 0 TO 10
READ armour$(i)
NEXT i
armourcount = INT(RND * 11)
armours$(npccount) = armour$(armourcount)
IF armourcount < 4 THEN GOSUB male
IF armourcount > 3 AND armourcount < 8 THEN GOSUB female
IF armourcount > 7 AND armourcount < 11 THEN GOSUB many
IF armourcount > 10 Then
GOTO equip
EndIf
acount = count

'МАССИВ ЩИТОВ - Все мужского рода. Начиная с 0'

shield:
DATA Баклер,Каплевидный щит,Скутум,Тарч,Умбон,Экю,Маленький щит,Большой щит,Щит
DIM shield$(8)
FOR i = 0 TO 8
READ shield$(i)
NEXT i
shieldcount = INT(RND * 9)
IF shieldcount = 0 OR shieldcount > 8 THEN GOTO jewelery
GOSUB male
scount = count


'МАССИВ УКРАШЕНИЙ 0-1мужской род 2-3женский род 4-5 - средний род'
jewelery:
DATA Талисман,Амулет,Корона,Серьга,Кольцо,Ожерелье
DIM jewelery$(5)
FOR i = 0 TO 5
READ jewelery$(i)
NEXT i
jewelerycount = INT(RND * 6)
IF jewelerycount < 2 THEN GOSUB male
IF jewelerycount > 1 AND jewelerycount < 4 THEN GOSUB female
IF jewelerycount > 3 THEN GOSUB middle

jcount = count

'МАССИВ ПОСТФИКСОВ'

postfix:
DATA Невидимости,Древних Королей,Безумства,Очарования,Подчинения,Предвидения,Скорости,Видения Невидимого,Выносливости,Героя,Воинов,Защиты,Храбрости,Удачи,Невезения,Защиты от Огня,Защиты от Холода,Защиты от Нежити
DIM postfix$(17)
FOR i = 0 TO 17
READ postfix$(i)
NEXT i
postfixcount = INT(RND * 18)
GOTO equip

'Проверка сгенерятся ли префикс,предмет,постфикс'

equip:
IF RND > .5 THEN itemprefix$(wcount) = ""
IF RND > .5 THEN postfix$(postfixcount) = ""
IF RND > .5 THEN weapon$(weaponcount) = "": GOTO equiparmour
IF itemprefix$(wcount) = "" THEN
equipweapon$(npccount) = weapon$(weaponcount) + " " + postfix$(postfixcount)
ELSE
equipweapon$(npccount) = itemprefix$(wcount) + " " + weapon$(weaponcount) + " " + postfix$(postfixcount)
END IF
equiparmour:
IF RND > .5 THEN itemprefix$(acount) = ""
IF RND > .5 THEN postfix$(postfixcount) = ""
IF RND > 5 THEN armour$(armourcount) = "": GOTO equipshield
IF itemprefix$(acount) = "" THEN
equiparmour$(npccount) = armour$(armourcount) + " " + postfix$(postfixcount)
ELSE
equiparmour$(npccount) = itemprefix$(acount) + " " + armour$(armourcount) + " " + postfix$(postfixcount)
END IF
equipshield:
IF RND > .5 THEN itemprefix$(scount) = ""
IF RND > .5 THEN postfix$(postfixcount) = ""
IF RND > .5 THEN shield$(shieldcount) = ""
IF itemprefix$(scount) = "" THEN
equipshield$(npccount) = shield$(shieldcount) + " " + postfix$(postfixcount)
ELSE
equipshield$(npccount) = itemprefix$(scount) + " " + shield$(shieldcount) + " " + postfix$(postfixcount)
END IF
IF shield$(shieldcount) = "" THEN
equipshield$(npccount) = ""
END IF
equipjewelery:
IF RND > .5 THEN itemprefix$(jcount) = ""
IF RND > .5 THEN postfix$(postfixcount) = ""
IF RND > .5 THEN jewelery$(jewelerycount) = ""
IF itemprefix$(jcount) = "" THEN
equipjewelery$(npccount) = jewelery$(jewelerycount) + " " + postfix$(postfixcount)
ELSE
equipjewelery$(npccount) = itemprefix$(jcount) + " " + jewelery$(jewelerycount) + " " + postfix$(postfixcount)
END IF
IF jewelery$(jewelerycount) = "" THEN
equipjewelery$(npccount) = ""
END IF

'ГЕНЕРАТОР КВЕСТОВ'

'Для оружия 0-7, Для людей 8-17, Для зелий 18-21''

DATA Найти,Спасти,Уничьтожить,Доставить,Вернуть,Зачаровать,Улучшить,Выковать
DATA Найти,Защитить,Сопроводить,Предупредить,Убить,Ограбить,Влюбить в себя,Завоевать дружбу,Спасти,Освободить из заключения
DATA Приготовить,Сварить,Выпить,Заставить принять
DIM quest$(21)
FOR i = 0 TO 21
READ quest$(i)
NEXT i
questtype = INT(RND * 3)
SELECT CASE questtype
'Создаем равные вероятности для генерации квестов разных типов, независимо от их кол-ва'
'0-квест+оружие,1-квест+профессия,2-квест+зелье'
CASE 0: quests$(questnumber) = quest$(INT(RND * 8)) + " " + weapon$(INT(RND * 8))
CASE 1: quests$(questnumber) = quest$(8 + INT(RND * 10)) + " " + prof$(44 + INT(RND * 22))
CASE 2: quests$(questnumber) = quest$(18 + INT(RND * 4)) + " " + item$(15 + INT(RND * 17))
END SELECT
otherchanges:
story$ = "Вы родились " + birthplace$(birthplacecount) + " в год " + year$(yearcount) + ". В детстве вы " + character$(charactercount) + ". У вас " + eyes$(eyescount) + " глаза." + " У вас " + body$(bodycount) + " тело. Вы " + race$(npccount) + "-"  _
+ profession$(0) + ", известный в этом мире под именем " + namepc$ + ". Ваша цель - " + quests$(0) + "."

'ПОДПРОГРАММА ВВОДА-ВЫВОДА'
'КОМАНДЫ ВЫВОДА ИНФОРМАЦИИ'


IF playgeneratedpc$ = "Да" THEN
GOTO pcscreen
ELSE
PRINT "Ваша раса"
INPUT race$(0)
PRINT "История и описание Вашего персонажа"
INPUT story$
PRINT "Ваша профессия"
INPUT profession$(0)
PRINT "Оружие"
INPUT equipweapon$(0)
PRINT "Доспехи"
INPUT equiparmour$(0)
PRINT "Щит"
INPUT equipshield$(0)
PRINT "Украшения"
INPUT equipjewelery$(0)
PRINT "Транспорт"
INPUT transports$(0)
PRINT "Инвентарь"
itemsamount=5
n = 1
FOR j = 1 TO 5:
PRINT "Предмет №"; n
INPUT itemss$(0, j)
if itemss$(0,j) ="" then 
itemsamount=itemsamount-1
end if
'Делаем последовательность, если были пропуски при вводе вещи в инвентарь'
for i=2 to 5
if itemss$(0,j)<>"" and itemss$(0,i-1)="" then
itemss$(0,i-1)=itemss$(0,j)
itemss$(0,j)=""
end if
next i
n = n + 1
NEXT j

PRINT "Сколько у вашего персонажа денег"
INPUT money(0)
END IF
playgeneratedpc$ = "Да"


pcscreen:
cls
wcolor = 7
acolor = 7
scolor = 7
jcolor = 7
IF equipweapon$(npccount) = "" THEN equipweapon$(npccount) = "(пусто)"
IF equiparmour$(npccount) = "" THEN equiparmour$(npccount) = "(пусто)"
IF equipshield$(npccount) = "" THEN equipshield$(npccount) = "(пусто)"
IF equipjewelery$(npccount) = "" THEN equipjewelery$(npccount) = "(пусто)"
equipweaponprint$ = equipweapon$(npccount) 'Вводим отдельную переменную, для печати в случае длинных названий'
equiparmourprint$ = equiparmour$(npccount)
equipshieldprint$ = equipshield$(npccount)
equipjeweleryprint$ = equipjewelery$(npccount)
PRINT TAB(33); "ОКНО ПЕРСОНАЖА": PRINT
PRINT TAB(35); "ЭКИПИРОВКА": PRINT
IF LEN(equipweaponprint$) > 28 THEN
equipweaponprint$ = LEFT$(equipweaponprint$, LEN(equipweaponprint$) - (LEN(equipweaponprint$) - 25)) + "..."
END IF
COLOR 7
PRINT "Оружие: ";
IF equipweaponprint$ = "(пусто)" THEN wcolor = 8
COLOR wcolor
PRINT TAB(11); equipweaponprint$;
IF LEN(equiparmourprint$) > 29 THEN
equiparmourprint$ = LEFT$(equiparmourprint$, LEN(equiparmourprint$) - (LEN(equiparmourprint$) - 26)) + "..."
END IF
COLOR 7
PRINT TAB(40); "Доспехи: ";
IF equiparmourprint$ = "(пусто)" THEN acolor = 8
COLOR acolor
PRINT TAB(51); equiparmourprint$
IF LEN(equipshieldprint$) > 28 THEN
equipshieldprint$ = LEFT$(equipshieldprint$, LEN(equipshieldprint$) - (LEN(equipshieldprint$) - 25)) + "..."
END IF
COLOR 7
PRINT "Щит: ";
IF equipshieldprint$ = "(пусто)" THEN scolor = 8
COLOR scolor
PRINT TAB(11); equipshieldprint$;
IF LEN(equipjeweleryprint$) > 29 THEN
equipjeweleryprint$ = LEFT$(equipjeweleryprint$, LEN(equipjeweleryprint$) - (LEN(equipjeweleryprint$) - 26)) + "..."
END IF
COLOR 7
PRINT TAB(40); "Украшения: ";
IF equipjeweleryprint$ = "(пусто)" THEN jcolor = 8
COLOR jcolor
PRINT TAB(51); equipjeweleryprint$
storyscreen:
COLOR 7
PRINT
PRINT TAB(35); "БИОГРАФИЯ": PRINT
PRINT story$: PRINT
PRINT TAB(35); "ИНВЕНТАРЬ": PRINT
n = 1
FOR j = 1 TO itemsamount
n$ = STR$(n)
IF n = 1 THEN
PRINT LTRIM$(n$) + ")" + " "; itemss$(npccount, j);
GOTO retreat
ELSE
PRINT n$ + ")" + " ";
PRINT itemss$(npccount, j);
retreat:
n = n + 1
END IF
NEXT j:
moneystring$ = STR$(itemsamount + 1) 'Добавляем для денег порядковый номер в инвентарь'
moneystringvault$ = STR$(money(npccount))
'Склоняем монету'
IF RIGHT$(moneystringvault$, 1) = "1" AND RIGHT$(moneystringvault$, 2) <> "11" THEN
coin$ = " золотая монета"
ELSEIF RIGHT$(moneystringvault$, 1) = "2" AND RIGHT$(moneystringvault$, 2) <> "12" OR RIGHT$(moneystringvault$, 1) = "3" AND RIGHT$(moneystringvault$, 2) <> "13" OR RIGHT$(moneystringvault$, 1) = "4" AND RIGHT$(moneystringvault$, 2) <> "14" THEN
coin$ = " золотые монеты"
ELSE
coin$ = " золотых монет"
END IF
IF itemsamount = 0 THEN
PRINT LTRIM$(moneystring$) + ")"; money(npccount); coin$: PRINT
ELSE
PRINT moneystring$ + ")"; money(npccount); coin$: PRINT
END IF
PRINT TAB(35); "ТРАНСПОРТ": PRINT
PRINT transports$(npccount)

'ВВОД КОМАНД'

start:

INPUT typecommands$
SELECT CASE typecommands$
CASE "Имя", "имя"
PRINT namepc$
CASE "Раса", "раса", "рас"
PRINT race$(0)
CASE "Профессия", "профессия", "про"
PRINT profession$(0)
CASE "Оружие", "оружие", "ору"
PRINT equipweapon$(0)
CASE "Доспехи", "доспехи", "дос"
PRINT equiparmour$(0)
CASE "Щит", "щит"
PRINT equipshield$(0)
CASE "Украшения", "украшения", "укр"
PRINT equipjewelery$(0)
CASE "Транспорт", "транспорт", "тра"
PRINT transports$(0)
CASE "Инвентарь", "инвентарь", "инв"
n = 1
FOR j = 1 TO itemsamount
PRINT LTRIM$(STR$(n)) + ")" + " "; itemss$(0, j)
n = n + 1
NEXT j
PRINT LTRIM$(STR$(itemsamount + 1)) + ")"; money(npccount); coin$
CASE "Деньги", "деньги", "ден"
PRINT money(0); coin$
CASE "Экипировка", "экипировка", "эки"
PRINT "Оружие: "; TAB(12); equipweapon$(npccount)
PRINT "Доспехи: "; TAB(12); equiparmour$(npccount)
PRINT "Щит: "; TAB(12); equipshield$(npccount)
PRINT "Украшения: "; TAB(12); equipjewelery$(npccount)
CASE "Персонаж", "персонаж", "пер"
GOTO pcscreen
CASE "Биография", "биография", "био"
PRINT story$

'КОМАНДЫ РЕДАКТИРОВАНИЯ ПЕРСОНАЖА'


CASE "Изменить расу", "Изменить расу", "изм рас": PRINT "Ваша раса": INPUT race$(0): GOSUB otherchanges
CASE "Изменить профессию", "изменить профессию", "изм про": PRINT "Ваша профессия": INPUT profession$(0): GOSUB otherchanges
CASE "Изменить оружие", "изменить оружие", "изм ору": PRINT "Оружие": INPUT equipweapon$(0): GOSUB pcscreen
CASE "Изменить доспехи", "изменить доспехи", "изм дос": PRINT "Доспехи": INPUT equiparmour$(0): GOSUB pcscreen
CASE "Изменить щит", "изменить щит", "изм щит": PRINT "Щит": INPUT equipshield$(0): GOSUB pcscreen
CASE "Изменить украшения", "изменить украшения", "изм укр": PRINT "Украшения": INPUT equipjewelery$(0): GOSUB pcscreen
CASE "Изменить деньги", "изменить деньги", "изм ден": PRINT "Сколько у вашего персонажа денег": INPUT money(0): GOSUB otherchanges
CASE "Изменить транспорт", "изменить транспорт", "изм тра": PRINT "Транспорт": INPUT transports$(0): GOSUB otherchanges
CASE "Добавить инвентарь", "добавить инвентарь", "доб инв"
PRINT
IF itemsamount+1 > 5 THEN PRINT "Вы не можете нести больше пяти предметов": GOTO start'Если пытаемся ввести больше 5 предметов'
IF itemsamount+1 < 6 THEN PRINT "Что добавляем?": itemsamount=itemsamount+1: INPUT itemss$(0,itemsamount)
goto pcscreen

CASE "Изменить инвентарь", "изменить инвентарь", "изм инв"
PRINT "Укажите номер вещи, которую требуется заменить"
INPUT inventorynumber
IF inventorynumber > itemsamount OR inventorynumber < 1 THEN
PRINT "Такого слота не существует"
GOTO start
END IF
PRINT "На что меняем?"
INPUT itemss$(0, inventorynumber)
goto pcscreen
CASE "Удалить инвентарь", "удалить инвентарь", "уда инв"
PRINT "Укажите номер вещи, которую вы собираетесь удалить"
INPUT inventorynumber
IF inventorynumber > itemsamount OR inventorynumber < 1 THEN PRINT "Неверный номер инвентаря": GOTO start
a = 0
FOR i = 1 TO itemsamount - inventorynumber
itemss$(0, inventorynumber + a) = itemss$(0, inventorynumber + 1 + a)
a = a + 1
NEXT i
itemsamount = itemsamount - 1
goto pcscreen
CASE "Изменить биографию", "изменить биографи", "изм био": GOSUB otherchanges
CASE "Изменить имя", "изменить имя", "изм имя": PRINT "Ваше новое имя?": INPUT namepc$: GOSUB otherchanges
CASE "Изменить цель", "изменить цель", "изм цел": PRINT "Укажите новую цель для вашего персонажа": INPUT quests$(0): GOsUB otherchanges

'СЛУЖЕБНЫЕ КОМАНДЫ'

CASE "Выход", "выход", "вых": END
CASE "заново": restore: goto begin


END SELECT
GOTO start

over:

END




